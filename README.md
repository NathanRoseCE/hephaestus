# Hephaestus

A force for setting up a computer. The idea is it is a mix of ansible
and aws cloudformation. Taking what I like from both projects and mixing
it into one service

## Why not ansible?

Ansible is great however there are a few things that I don't like about 
it

1. I do not trust ibm to maintain thier promises after the RHEL/Centos
schenanigans
2. Ansible does not have the ability to clean up and partial deletions,
This is critical as I would like the ability to modify my infrastructure
and not have to maintain seperate cleanup playbooks for each role
3. Lack of Turing completeness

## Why not cloudformation
1. AWS is expensive
2. It is optimized for cloud deployments, could hack something together
with custom resources, but 100% not worth it


## Broad idea

Create a package that get's installed on the host, this will have to be
done beforehand. It will then take in a deployment configuration which
will have a name and set of resources associated with that configuration
that configuration will then be setup on the host machine. 

Right now the configuration will be a json file(because json > yaml) and
the nodes will be hand crafted by me. Will have to create a way to
1. allow modules to be loaded for custom functionality
2. allow downloading of other modules
