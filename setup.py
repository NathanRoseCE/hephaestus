from distutils.core import setup
from distutils.command.install_data import install_data


class post_install(install_data):
    def run(self):
        # call parent
        install_data.run(self)

        
setup(
    name="Hephaestus",
    version="1.0.0-alpha",
    description="Infrastructure setup utility",
    author="Nathan Rose",
    author_email="mail.nathanrose@gmail.com",
    packages=["distutils", "distutils.command"],
    cmdclass={"install": post_install}
)
