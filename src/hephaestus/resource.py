import logging
from abc import ABC, abstractmethod
import os
import time
from typing import Dict, Any

from src.hephaestus.constants import LOG_FOLDER, LOG_TIMESTAMP_FORMAT


class Resource(ABC):
    """
    This is the base class for a resource.
    It's idea is that it is a deployable unit that can be created when needed.

    These can and should be composite's of others where needed.

    If there is a failure it is indicated by throwing an exception.
    """

    def __init__(self, deployment_name: str, resource_name: str) -> None:
        self._deployment_name = deployment_name
        self._resource_name = resource_name

        self._logger = logging.getLogger(f"{deployment_name}.{resource_name}")
        self._log_file = os.path.join(
            LOG_FOLDER,
            deployment_name,
            resource_name,
            f'{time.strftime(LOG_TIMESTAMP_FORMAT)}.log'
        )
        self._logger.addHandler(logging.FileHandler(self._log_file))

    def create(self, new_properties: Dict[str, Any]) -> None:
        self._logger.info("Deployment(%s): Creating resource '%s' with properties: %s",
                          self._deployment_name,
                          self._resource_name,
                          new_properties)
        self._create(new_properties)

    @abstractmethod
    def _create(self, new_properties: Dict[str, Any]) -> None:
        """Overload for creation logic"""

    def update(self, new_properties: Dict[str, Any], old_properties: Dict[str, Any]) -> None:
        self._logger.info("Deployment(%s): Creating resource '%s' with new properties: %s, old: %s",
                          self._deployment_name,
                          self._resource_name,
                          new_properties,
                          old_properties
                          )
        self._update(new_properties, old_properties)

    @abstractmethod
    def _update(self, new_properties: Dict[str, Any], old_properties: Dict[str, Any]) -> None:
        """Called when the resource already exists, but properties have changed."""

    @abstractmethod
    def delete(self, old_properties: Dict[str, Any]) -> None:
        """Called when the resource currently exists but has been deleted."""
        self._logger.info("Deployment(%s): Creating resource '%s' with old properties: %s",
                          self._deployment_name,
                          self._resource_name,
                          old_properties
                          )
        self._delete(old_properties)

    @abstractmethod
    def _delete(self, old_properties: Dict[str, Any]) -> None:
        """Called when the resource already exists, but properties have changed."""

        
